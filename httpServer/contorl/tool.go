package contorl

import (
	"fmt"
	"goAdapter/device"
	"goAdapter/httpServer/model"
	"net/http"

	"github.com/gin-gonic/gin"
)

func ApiGetCommMessage(context *gin.Context) {
	interfaceName := struct {
		CollInterfaceNames []string `json:"CollInterfaceNames"` //接口名称
	}{}

	commMessageMap := make([]device.CommunicationMessageTemplate, 0)

	err := context.ShouldBindJSON(&interfaceName)
	if err != nil {
		fmt.Println("interfaceInfo json unMarshall err,", err)

		context.JSON(http.StatusOK, model.Response{
			Code:    "1",
			Message: "json unMarshall err",
		})
		return
	}

	for _, name := range interfaceName.CollInterfaceNames {
		coll, ok := device.CollectInterfaceMap.Coll[name]
		if !ok {
			continue
		}
		commMessageMap = append(commMessageMap, coll.CommQueueManage.CommMessage...)
		// 清空map
		coll.CommQueueManage.CommMessage = coll.CommQueueManage.CommMessage[0:0]
	}

	context.JSON(http.StatusOK, model.ResponseData{
		Code: "0",
		Data: commMessageMap,
	})
}
